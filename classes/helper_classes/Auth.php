<?php
class Auth
{
    protected $database;
    protected $hash;
    private $di;
    protected $table = "users";
    protected $authSession = "user";
    
    /*
        Auth constructor.
        @param $database
    */
    public function __construct(DependencyInjector $di)
    {
        $this->di =$di;
        $this->database = $this->di->get('database');
        $this->hash = $this->di->get('hash');
    }
    public function build(){
        $query = "CREATE TABLE IF NOT EXISTS {$this->table} (id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, email VARCHAR(255) NOT NULL UNIQUE,username VARCHAR(255) NOT NULL UNIQUE,password VARCHAR(255) NOT NULL)";
        return $this->database->query($query);
    }
    public function create($data)
    {
        if(isset($data['password']))
        {
            $data['password'] = $this->hash->make($data['password']);
        }
        return $this->database->insert($this->table,$data);
    }
    public function signin($data): bool
    {
        $this->database->table($this->table)->where('username','=',$data['username']);
        if($this->database->count()==1)
        {
    
            $user = $this->database->first();
        
            if($this->hash->verify($data['password'],$user->password))
            {
                
                $this->setAuthSession($user->id);
                return true;
            }
            
        }
        return false;
    }
    public function setAuthSession($id)
    {
        $_SESSION[$this->authSession] = $id;
    }
    public function unsetAuthSession()
    {   
        unset($_SESSION[$this->authSession]);
    }
    public function check()
    {
        return isset($_SESSION[$this->authSession]);
    }
    public function checkAuthor(){
        $result = $this->getAuthorById($_SESSION[$this->authSession]);
        if($result == null){
            return false;
        }else{
            return true;
        }
    }
    public function user()
    {
        if(!$this->check())
        {
            return false;
        }
        $user = $this->database->table($this->table)->where('id','=',$_SESSION[$this->authSession])->first();
        return $user;
        
    }
    public function getUserById(string $id)
    {
        return $this->database->table($this->table)->where('id','=',$id)->first();
    }
    public function getUserByEmail(string $email)
    {
        return $this->database->table($this->table)->where('email','=',$email)->first();
    }
    public function getUserByUsername(string $username)
    {
        return $this->database->table($this->table)->where('username','=',$username)->first();
    }
    public function resetUserPassword(string $token,string $password)
    {
        $password = $this->hash->make($password);
        $sql = "UPDATE users, tokens SET users.password = '{$password}' WHERE users.id = tokens.user_id AND tokens.token = '{$token}'";
        return $this->database->query($sql);
    }
    public function signout()
    {
        setcookie('token','',time()-5000);
        $user_id = $_SESSION[$this->authSession];
        $sql = "DELETE FROM tokens WHERE user_id = {$user_id} and is_remember=1";
        $this->database->query($sql);
        $this->unsetAuthSession();
    }
    public function getAuthorById($id){
        return $this->database->table('author')->where('user_id','=',$id)->first();
    }
    public function getAuthorByItsId($id){
        return $this->database->table('author')->where('id','=',$id)->first();
    }
    public function updateUser(string $table,$data,$condition,$img_name){
        
        $data_to_be_updated = [
            'email'=>$data['email'],
            'username'=>$data['username'],
            'first_name'=>$data['first_name'],
            'last_name'=>$data['last_name'],
            'user_img'=>$img_name
        ];
        $result = $this->database->update($table,$data_to_be_updated,$condition);
        if($result){
            return UPDATE_PROFILE;
        }else{
            return UPDATE_ERROR;
        }
    }
    public function subscribe($id){
        $user = $this->getUserById($id);
        $data_to_be_inserted = [
            'user_id'=>$user->id,
            'username'=>$user->username,
            'user_img'=>$user->user_img
        ];
        $result = $this->database->insert('author',$data_to_be_inserted);
        if($result){
            return ADD_AUTHOR;
        }else{
            return ADD_ERROR;
        }
    }
    
}