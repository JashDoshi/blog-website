<?php
class Comment
{
	private $table = "comments";
	private $columns = ['post_id','author_id','user_id','comment','pending','created_at','updated_at'];
	protected $di;
    private $database;
    private $auth;
	private $validator;
	public function __construct(DependencyInjector $di){
		$this->di = $di;
        $this->database = $this->di->get('database');
        $this->auth = $this->di->get('auth');
	}
	public function getValidator()
	{
		return $this->validator;
	}
	public function validateData($data)
	{

		$this->validator = $this->di->get('validator');
		$this->validator = $this->validator->check($data,[
			'comment'=>[
				'required'=>true
			]
		]);
	}
	public function addComment($data)
	{
		//validate data
		$this->validateData($data);

		//insert data in database
		if(!$this->validator->fails())
		{
			try
			{
				$currentTime = date("Y-m-d H:i:s");
				$this->database->beginTransaction();
				$data_to_be_inserted = [
					'post_id'=>$data['post_id'],
					'author_id'=>$data['author_id'],
					'user_id'=>$data['user_id'],
					'comment'=>$data['comment_content'],
					'created_at'=>date("Y-m-d H:i:s",strtotime($currentTime)),
					'updated_at'=>date("Y-m-d H:i:s",strtotime($currentTime))

				];
				$customer_id = $this->database->insert($this->table,$data_to_be_inserted);
				$this->database->commit();
				return ADD_SUCCESS;
			}catch(Exception $e){
				$this->database->rollBack();
				return ADD_ERROR;
			}
		}
		return VALIDATION_ERROR;
	}
	public function getJSONDataForDataTable($draw,$search_parameter,$start,$length)
	{
        $author = $this->auth->getAuthorById(Session::getSession('user'));
		$dummy = "SELECT * FROM {$this->table} WHERE author_id = $author->id AND deleted = 0 AND pending = 0";
		$dummyResult = $this->database->raw($dummy);

		$query = "SELECT comment,post_heading,comments.id,users.username,users.email FROM {$this->table},posts,users WHERE users.id = comments.user_id AND posts.id = comments.post_id AND comments.author_id = $author->id AND comments.deleted = 0 AND comments.pending = 0";
   
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0 and pending = 0";
		$filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0 and pending = 0";

		if($search_parameter != null)
		{
			$query .= " AND comment LIKE '%{$search_parameter}%'";
			$filteredRowCountQuery.=" AND comment LIKE '%{$search_parameter}%'";
		}

		

		if($length != -1)
		{
			$query .= " LIMIT {$start}, {$length}";
		}

		$totalRowCountResult = $this->database->raw($totalRowCountQuery);
		$numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

		$filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
		$numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;
        
		$fetchData = $this->database->raw($query);
		$data = [];
		$numRows = is_array($fetchData) ? count($fetchData) : 0;
		for($i=0;$i<$numRows;$i++)
		{
			$subArray = [];
			$subArray[] = $start+$i+1;
			$subArray[] = $fetchData[$i]->post_heading;
			$subArray[] = $fetchData[$i]->username;
			$subArray[] = $fetchData[$i]->comment;
			$subArray[] = <<<BUTTONS
<button class='btn btn-outline-primary btn-sm accept'  data-toggle="modal" data-id='{$fetchData[$i]->id}'><i class="fa fa-check"></i></button> 
<button class='btn btn-outline-danger btn-sm reject' data-id='{$fetchData[$i]->id}' data-name='{$author->username}' data-email='{$fetchData[$i]->email}'>X</button> 
BUTTONS;
			$data[] = $subArray;
		}
		$output = array(
			'draw'=>$draw,
			'recordsTotal'=>$numberOfTotalRows,
			'recordsFiltered'=>$numberOfFilteredRows,
			'data'=>$data
        );
        
		echo json_encode($output);
    }
    
    public function acceptComment($id){
        try
        {
            $this->database->beginTransaction();
            $data_to_be_updated = ['pending'=>"1"];
            $this->database->update($this->table,$data_to_be_updated,"id = {$id}");
			$this->database->commit();
			echo $id;
            return UPDATE_SUCCESS;
        }
        catch(Exception $e)
        {
            $this->rollBack();
            return UPDATE_ERROR;
        }
	}
	public function rejectComment($id){
        try
        {
            $this->database->beginTransaction();
            $data_to_be_updated = ['deleted'=>"1"];
            $this->database->update($this->table,$data_to_be_updated,"id = {$id}");
			$this->database->commit();
			echo $id;
            return UPDATE_SUCCESS;
        }
        catch(Exception $e)
        {
            $this->rollBack();
            return UPDATE_ERROR;
        }
	}
	public function getAcceptedComment($id,$post_id){
        $start = Session::getSession("startComment");
        $max = 2;
        $change = Session::getSession("startComment")+$max;
        Session::setSession("startComment",$change);
        $query = "SELECT * FROM comments WHERE pending = 1 AND deleted = 0 AND author_id = $id AND post_id = $post_id LIMIT $start, $max";
        
        return $this->database->raw($query);
    }
    public function getUserByComment($result){
        $start = Session::getSession("startComment")-2;
        $max = 2;
        $change = Session::getSession("startComment")+$max-2;
        $resultArray = [];
        foreach($result as $row){
            
            $query = "SELECT users.username,users.user_img,comments.comment,comments.updated_at,comments.id FROM comments,users WHERE users.id = $row->user_id AND pending = 1 AND deleted = 0 AND author_id = $row->author_id AND post_id = $row->post_id LIMIT $start, 1";
            $start = $start+1;
            $resultArray[] =  $this->database->raw($query);
            
        }
        
        return $resultArray;
    }
    public function getPendingComment($id){
        $query = "SELECT * FROM comments WHERE pending = 0 AND deleted = 0 AND author_id = $id";
        return $this->database->raw($query);
    }
}