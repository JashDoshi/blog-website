<?php
class Post
{
	private $table = "posts";
	private $columns = ['id','author_id','heading','content','img_name','created_at','updated_at'];
	protected $di;
	private $database;
	private $validator;
	public function __construct(DependencyInjector $di){
		$this->di = $di;
		$this->database = $this->di->get('database');
	}
	public function getValidator()
	{
		return $this->validator;
	}
	public function validateData($data)
	{

		$this->validator = $this->di->get('validator');
		$this->validator = $this->validator->check($data,[
			'post_heading'=>[
				'required'=>true
			],
			'post_content'=>[
				'required'=>true
			]
		]);
	}
	public function addPost($data,$image_name)
	{
		//validate data
		$this->validateData($data);

		//insert data in database
		if(!$this->validator->fails())
		{	
			try
			{
				$currentTime = date("Y-m-d H:i:s");
				$this->database->beginTransaction();
				$data_to_be_inserted = [
					'author_id'=>$data['author_id'],
					'post_heading'=>$data['post_heading'],
					'post_content'=>$data['post_content'],
					'img_name'=>$image_name,
					'created_at'=>date("Y-m-d H:i:s",strtotime($currentTime)),
					'updated_at'=>date("Y-m-d H:i:s",strtotime($currentTime))

				];
				$post_id = $this->database->insert($this->table,$data_to_be_inserted);
				$category_id = $data['category'];
				$data_for_post_category = [
					'post_id'=>$post_id,
					'category_id'=>$category_id
				];
				$post_category_id = $this->database->insert('post_category',$data_for_post_category);
				$tags = explode(",",$data['post_tag']);
				foreach($tags as $tag){
					$newtag = "#".$tag;
					$tag_id = $this->database->insert('tags',[
						'tag_name'=>$newtag
					]);
					$post_tag_id = $this->database->insert('post_tag',[
						'post_id'=>$post_id,
						'tag_id'=>$tag_id
					]);
				}
				$this->database->commit();
				return ADD_SUCCESS;
			}catch(Exception $e){
				$this->database->rollBack();
				return ADD_ERROR;
			}
		}
		return VALIDATION_ERROR;
	}
	public function getPostCount($id){
        return $this->database->table('posts')->where("author_id","=",$id)->count();
	}
	public function getPosts($id){
        
        $query = "SELECT * FROM posts WHERE author_id = $id and deleted = 0 ORDER BY updated_at DESC";
        
        return $this->database->raw($query);
    }
    public function getPostById($id){
        $query = "SELECT * FROM posts WHERE id = $id AND deleted = 0";
        return $this->database->raw($query);
    }
    public function getAllPosts(){
        $start = Session::getSession("startPost");
        $max = Session::getSession("maxPost");
        $change = Session::getSession("startPost")+Session::getSession("maxPost");
        Session::setSession("startPost",$change);
        $query = "SELECT * FROM author,posts WHERE posts.author_id=author.id and posts.deleted = 0 ORDER BY updated_at DESC LIMIT $start, $max";
        
        return $this->database->raw($query);
	}
	public function getPostByCategory($category_name){
		$category = $this->database->raw("SELECT * FROM category WHERE category_name = '$category_name'");
		
		$start = Session::getSession("startPost");
        $max = Session::getSession("maxPost");
        $change = Session::getSession("startPost")+Session::getSession("maxPost");
        Session::setSession("startPost",$change);
        $query = "SELECT * FROM author,posts INNER JOIN (SELECT post_id as post_id FROM post_category WHERE category_id = {$category[0]->id}) as temp ON posts.id = temp.post_id WHERE posts.author_id=author.id and posts.deleted = 0 ORDER BY updated_at DESC LIMIT $start, $max";
        
        return $this->database->raw($query);
	}
	public function getAllCategory(){
		$query = "SELECT * FROM category";
		return $this->database->raw($query);
	}
    public function updatePost(string $table,$data,$condition,$img_name){
        $currentTime = date("Y-m-d H:i:s");
        $data_to_be_updated = [
            'post_heading'=>$data['post_heading'],
            'post_content'=>$data['post_content'],
            'img_name'=>$img_name,
            'updated_at'=>date("Y-m-d H:i:s",strtotime($currentTime))
        ];
        $result = $this->database->update($table,$data_to_be_updated,$condition);
        if($result){
            return UPDATE_POST;
        }else{
            return UPDATE_ERROR;
        }
	}
	public function getAllTags($id){
		$query = "SELECT tags.tag_name FROM tags INNER JOIN (SELECT post_tag.tag_id as tag_id FROM post_tag WHERE post_id = {$id}) as temp ON tags.id = temp.tag_id";
		return $this->database->raw($query);
	}
}