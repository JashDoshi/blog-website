<?php
require_once "../../helper/init.php";
$emailplaceholder = "Email";
if(!empty($_POST))
{
//    die(var_dump($_POST));
    $email = $_POST['email'];
    $rules = [
        "email"=>[
            "required"=>true,
            "email"=>true
        ]
    ];
    $validation = $di->get('validator')->check($_POST,$rules);
    if($validation->fails())
    {
        if($di->get('validator')->errors()->first('email') != null){
            $emailplaceholder = $di->get('validator')->errors()->first('email');
            $emailclass = "required";
        }else{
            $emailclass = "";
        }
    }
    
    else{
        $user = $di->get('auth')->getUserByEmail($email);
        if($user)
        {
            $token = $di->get('token_handler')->createForgotPasswordToken($user->id);
            if($token)
            {
                $body = "<p>Use the below link within 15 mins to reset your password.</p>";
                $body .= "<p><a href='http://localhost:8000/views/auth/reset-password.php?token={$token}&email={$email}'>RESET PASSWORD</a></p>";

                $mail->addAddress($user->email);
                $mail->Subject = "Reset Password";
                $mail->Body = $body;

                if($mail->send())
                {
                    echo "Password reset link has been sent";
                }
                else
                {
                    echo $mail->ErrorInfo;
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/style.css">
</head>
<body class="body">
    <div class="forgot-pass">
        <div class="row">
            <div class="col-md-12">
                <div class="logo-img">
                    <img src="<?=BASEASSETS?>images/blog-logo.png" alt="">
                </div>
            </div>
            
        </div>
        <form action="forgot-password.php" method="POST">
            <legend class="text-center">Forgot Password</legend>
            <label class="email">
                <input type="text" name="email" class="<?=$emailclass;?>" placeholder="<?=$emailplaceholder;?>">
            </label>
            <input type="submit" class="btn-my" value="Forgot Password">
        </form>
    </div>
</body>
</html>