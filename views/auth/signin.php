<?php
require_once "../../helper/init.php";
Session::unsetSession('skip');
$userplaceholder = "Username";
$passplaceholder = "Password";
$wrong = false;
if(!empty($_POST))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    if(isset($_POST['remember_me'])){
        $rememberMe = $_POST['remember_me'];
    }
    
    $validation = $di->get('validator')->check($_POST,[
        "username"=>[
            "required"=>true
        ],
        "password"=>[
            "required"=>true,
            "minlength"=>8
        ],
    ]);
    
    if($di->get('validator')->fails())
    {
//        echo "<pre>" . print_r($di->get('validator')->errors()->all())."</pre>";
//        $user-error =  $di->get('validator')->errors()->first('username');
//        $pass-error =  $di->get('validator')->errors()->first('password');
        
        if($di->get('validator')->errors()->first('username')!=null){
            $userplaceholder = $di->get('validator')->errors()->first('username');
            $userclass = "required";
        }else{
            $userclass = "";
        }
        if($di->get('validator')->errors()->first('password') != null){
            $passplaceholder = $di->get('validator')->errors()->first('password');
            $passclass = "required";
        }else{
            $passclass = "";
        }
    }
    else
    {
        $signin = $di->get('auth')->signin([
            'username'=>$username,
            'password'=>$password
        ]);
        if($signin)
        {
            if($rememberMe)
            {
                $token = $di->get('token_handler')->createRememberMeToken($di->get('auth')->getUserByUsername($username)->id);
                setcookie('token',$token,time()+$di->get('token_handler')::REMEMBER_EXPIRY_TIME_IN_SECONDS);

            }
            header("Location: ../pages/index.php");
        }else{
            $wrong = true;
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign In</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS?>styles/style.css">
</head>
<body class="body">
    <div class="signin">
        <div class="row ">
            <div class="col-md-12">
                <div class="logo-img">
                    <img src="<?=BASEASSETS?>images/blog-logo.png" alt="">
                </div>
                <p class="float-right"><a class="skip" href="<?=BASEPAGES?>index.php">Skip</a></p>
            </div>
            
        </div>
        <div class="input-fields">
            
            <form action="" method="POST">
                <legend class="text-center">Sign In</legend>
                <label class="username">
                    <input type="text" name="username" class="<?=$userclass;?>" placeholder="<?=$userplaceholder;?>">
                </label>
                <label class="pass">
                    <input type="password" name="password" class="<?=$passclass;?>" placeholder="<?=$passplaceholder;?>"> 
                </label>
<?php
            if($wrong):
?>
                <p class="wrong">Wrong Username or Password</p>
<?php
            $wrong = true;
            endif;
?>
                <label style="display: inline-block; width:70%;margin: 0 15%;margin-top:1rem;">
                    Remember me
                    <input type="checkbox" name="remember_me" checked="on">
                </label>                
                <input type="submit" class="btn-my" value="Sign In">
                <p class="forgot text-center">Forgot <a href="forgot-password.php">Password?</a></p>
                <p class="signup-txt text-center">Create an account? <a href="signup.php">Sign Up</a></p>
            </form>
        </div>
    </div>
    <script src="<?=BASEASSETS?>scripts/jquery.js"></script>
    <script src="<?=BASEASSETS?>scripts/auth/script.js"></script>
</body>
</html>