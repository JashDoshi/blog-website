<?php
require_once "../../helper/init.php";
if(!empty($_POST))
{
    $email = $_POST['email'];
    $token = $_POST['token'];
    $password = $_POST['password'];
    if($di->get('token_handler')->isValid($token,0))
    {
        $password_reset_flag = $di->get('auth')->resetUserPassword($token,$password);
        $token_delete_flag = $di->get('token_handler')->deleteToken($token);
        if($password_reset_flag && $token_delete_flag)
        {
            header("Location: signin.php");
        }
        else
        {
            echo "Sorry, there was some issue while updating password";    
        }
    
    }
    else
    {
        echo "Your time to reset has expired";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reset Password</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/style.css">
</head>
<body class="body">
<?php
if(isset($_GET['token']) && isset($_GET['email'])):
    $token = $_GET['token'];
    $email = $_GET['email'];
    if($di->get('token_handler')->isValid($token,0)):
?>
    <div class="reset-pass">
        <div class="row">
            <div class="col-md-12">
                <div class="logo-img">
                    <img src="<?=BASEASSETS?>images/blog-logo.png" alt="">
                </div>
            </div>
            
        </div>
        <form action="" method="POST">
            <legend class="text-center">Reset Password</legend>
            <input type="hidden" name="token" value="<?=$token; ?>">
            <label class="email">
                <input type="text" name="email" value="<?= $email; ?>">
            </label>
            <label class="pass">
                <input type="password" name="password" placeholder="New Password">
            </label>
            <input type="submit" class="btn-my" value="Reset Password">
        </form>
    </div>
<?php
    else:
        echo "<p>your token time has expired</p>";
    endif;
else:
    echo "<p>How did you reach here??</p>";
endif;
?>
</body>
</html>