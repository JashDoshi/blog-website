<?php
require_once "../../helper/init.php";

$userplaceholder = "Username";
$passplaceholder = "Password";
$emailplaceholder = "Email";
if(!empty($_POST))
{
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rules = [
        "email"=>[
            "required"=>true,
            "maxlength"=>255,
            "unique"=>"users",
            "email"=>true
        ],
        "username"=>[
            "required"=>true,
            "minlength"=>2,
            "maxlength"=>20,
            "unique"=>"users"
        ],
        "password"=>[
            "required"=>true,
            "minlength"=>8,
            "maxlength"=>255
        ],
    ];
    $validation = $di->get('validator')->check($_POST,$rules);
    if($validation->fails())
    {
//        echo "<pre>" . print_r($validation->errors()->all())."</pre>";
        if($di->get('validator')->errors()->first('username')!=null){
            $userplaceholder = $di->get('validator')->errors()->first('username');
            $userclass = "required";
        }else{
            $userclass = "";
        }
        if($di->get('validator')->errors()->first('password') != null){
            $passplaceholder = $di->get('validator')->errors()->first('password');
            $passclass = "required";
        }else{
            $passclass = "";
        }
        if($di->get('validator')->errors()->first('email') != null){
            $emailplaceholder = $di->get('validator')->errors()->first('email');
            $emailclass = "required";
        }else{
            $emailclass = "";
        }
    }
    else
    {
        $created = $di->get('auth')->create([
            'email'=> $email,
            'username'=>$username,
            'password'=>$password
        ]);
        if($created)
        {
            header("Location: ../pages/index.php");
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/style.css">
</head>
<body class="body">
    <div class="signup">
        <div class="row">
            <div class="col-md-12">
                <div class="logo-img">
                    <img src="<?=BASEASSETS?>images/blog-logo.png" alt="">
                </div>
            </div>
            
        </div>
        <form action="signup.php" method="POST">
            <legend class="text-center">Sign Up</legend>
            <label class="email">
                <input type="text" name="email" class="<?=$emailclass;?>" placeholder="<?=$emailplaceholder;?>">
            </label>
            <label class="username">
                <input type="text" name="username" class="<?=$userclass;?>" placeholder="<?=$userplaceholder;?>">
            </label>
            <label class="pass">
                <input type="password" name="password" class="<?=$passclass;?>" placeholder="<?=$passplaceholder;?>">
            </label>
            <input type="submit" class="btn-my" value="Sign Up">
            <p class="forgot text-center">Already Registered? <a href="signin.php">Sign In</a></p>
        </form>
    </div>
</body>
</html>