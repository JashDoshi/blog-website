<?php
require_once "../../helper/init.php";
$di->get('auth')->signout();
header("Location: ../pages/index.php");