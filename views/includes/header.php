<header class="header">
    <div class="row">
        <div class="col-md-2">
            <p>Blogger</p>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <ul class="header-links">
                <li class="text-center header-link">
                    <a class="dashboard-link" href="<?=BASEPAGES;?>">
                        <span>Home</span>
                    </a>
                </li>
                <li class="text-center header-link">
                    <a class="dashboard-link" href="#">
                        <span>About us</span>
                    </a>
                </li>
                <li class="text-center header-link">
                    <a class="dashboard-link" href="#">
                        <span>Contact us</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
    <?php
        if(isset($skip)):
    ?>    
            <a class="btn-index" href="<?=BASEAUTH;?>signin.php">Signin</a>
    <?php
        else:
            $user = $di->get('auth')->getUserById(Session::getSession('user'));
    
    ?>
            <a class="btn-profile float-right" href="<?=BASEPAGES;?>profile.php">
                
                <img src="<?=BASEASSETS;?>images/users/<?=$user->user_img?>" alt="">
            </a>
    <?php
        endif;
    ?>
        </div>
    </div>
    

</header>