<ul class="navbar-nav bg-gradient-primary sidebar accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Dashboard -->
    <!-- <li class="nav-item text-center <?= $sidebarSection =='home' ? 'active' : '';?> ">
        <a class="nav-link dashboard-link <?=$active == 'index' ? 'active-link' : ''?>" href="<?=BASEPAGES;?>">
            <span>Home</span>
        </a>
    </li> -->
<?php
    $author = $di->get('auth')->getAuthorById(Session::getSession($authSession));
    if($author!=null):
?>
        <li class="nav-item m-10 text-center">
            <a class="nav-link dashboard-link <?=$active == 'add-post' ? 'active-link' : ''?>" href="<?=BASEPAGES;?>add-post.php">
                <span>Add Post</span>
            </a>
        </li> 
        <li class="nav-item m-10 text-center">
            <a class="nav-link dashboard-link <?=$active == 'manage-post' ? 'active-link' : ''?>" href="<?=BASEPAGES;?>manage-post.php">
                <span>Manage Post</span>
            </a>
        </li>     
        <li class="nav-item m-10 text-center">
<?php
        $comments = $di->get('comment')->getPendingComment($author->id);
        $count = (count($comments));
?>
            <a class="nav-link dashboard-link  <?=$active == 'show-comment' ? 'active-link' : ''?>" href="<?=BASEPAGES;?>show-comments.php">
            <span>Comments</span>
<?php 
            if($count>0):
?>
                <span class="count-comment"><?=$count?></span>
<?php
            endif;
?>
                
            </a>
        </li>   
<?php
    endif;
?>

</ul>