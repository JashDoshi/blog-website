<script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
<script src="<?=BASEASSETS;?>scripts/plugins/toastr/toaster.min.js"></script>
<script src="<?=BASEASSETS;?>scripts/pages/index.js"></script>

<script>
    
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
    }
	<?php
		if(Session::hasSession(UPDATE_COMMENT)):
	?>
			toastr.success("Comment has added to your post","Added");
	<?php
			Session::unsetSession(UPDATE_COMMENT);
		elseif(Session::hasSession(ADD_ERROR)):
	?>
			toastr.error("Adding a category failed","Failed");
	<?php
		    Session::unsetSession(ADD_ERROR);
		elseif (Session::hasSession(UPDATE_SUCCESS)):
	?>
			toastr.success("cateory has been updated successfully","Updated");
	<?php	
		    Session::unsetSession(UPDATE_SUCCESS);
		
		elseif(Session::hasSession('csrf')):
	?>
			toastr.error("Unauthorized Access,Token match","Token error!","Error");
	<?php
            Session::unsetSession('csrf');
        elseif (Session::hasSession(UPDATE_ERROR)):
    ?>
            toastr.error("Updation failed","Failed");
    <?php
            Session::unsetSession(UPDATE_ERROR);
        elseif(Session::hasSession(UPDATE_PROFILE)):
    ?>
            toastr.success("Profile updated successfully","Updated");
    <?php
            Session::unsetSession(UPDATE_PROFILE);
     
        elseif(Session::hasSession(ADD_POST)):
    ?>
            toastr.success("Your Post has added successfully","Added");
    <?php
            Session::unsetSession(ADD_POST);
        elseif(Session::hasSession(ADD_COMMENT)):
    ?>
            toastr.success("Your Comments has submitted successfully","Added");
    <?php
			Session::unsetSession(ADD_COMMENT);
			elseif(Session::hasSession(REJECT_COMMENT)):
	?>
			toastr.success("Comments rejected successfully","Rejected");
	<?php
			Session::unsetSession(REJECT_COMMENT);
			elseif(Session::hasSession(UPDATE_POST)):
	?>
			toastr.success("Post updated successfully","Updated");
	<?php
			Session::unsetSession(UPDATE_POST);
			elseif(Session::hasSession(ADD_AUTHOR)):
	?>
			toastr.success("You are now an author","Subscribed");
	<?php
			Session::unsetSession(ADD_AUTHOR);
       	 	endif;
    ?>
</script>