<?php
require_once "../../helper/init.php";
$authSession = "user";
Util::createCSRFToken();

if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}

$active = "show-comment";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>vendor/datatables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>vendor/fontawesome/css/font-awesome.min.css">

</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(!isset($skip) and $di->get('auth')->checkAuthor()):	   
?>
<?php
    $user = $di->get('auth')->getUserById(Session::getSession('user'));
    
    
?>    
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
  
    <div class="main-card">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="container-fluid">
                    <h1 class="h3 mb-4 text-gray-888">Manage Comments</h1>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Comments</h6>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-responsive" id="manage-comment-table">
                                <div id="export-buttons"></div>
                                <thead>

                                    <tr>
                                        <th>#</th>
                                        <th>Post Heading</th>
                                        <th>Comment By</th>
                                        <th>Comment</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
<?php 
else: 
    if(!isset($skip)){
        header("Location: http://localhost:8000/views/pages/index.php");
    }else{
        header("Location: http://localhost:8000/views/auth/signin.php");
        exit();
    }
 ?>
<?php endif; ?>

    <script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
    <?php
        require_once __DIR__."/../includes/page-level/manage-comments-scripts.php";
    ?>
</body>
</html>