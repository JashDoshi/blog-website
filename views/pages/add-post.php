<?php
require_once "../../helper/init.php";
$authSession = "user";
Util::createCSRFToken();
$errors = "";
$old = "";
$active = "add-post";
if(Session::hasSession('old'))
{

  $old = Session::getSession('old');
  Session::unsetSession('old');
}
if(Session::hasSession('errors'))
{
  // Util::dd(Session::getSession('errors'));
  $errors = unserialize(Session::getSession('errors'));
  
  Session::unsetSession('errors');
}
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
</head>
<body>
<?php
if(!isset($skip) and $di->get('auth')->checkAuthor()):	   
?>
      
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
    
    <div class="main-card">
        <div class="row m-0">
            <div class="col-md-8 middle">
                <h3 style="margin-bottom:20px;">Add Post</h3>
            </div>
        </div>  
            
        <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-category" enctype="multipart/form-data">
            <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
            
            <input type="hidden" name="author_id" value="<?=$di->get('auth')->getAuthorById(Session::getSession($authSession))->id;?>">
            <div class="row m-0">
                <div class="col-md-8 p-0 middle">
                    <div class="form-group">
                        
                        <label>Post Heading</label>
                        
                        <textarea name="post_heading" placeholder="Enter post heading" id="post_heading" class="form-control <?=$errors!='' ? ($errors->has('post_heading') ? 'error is-invalid' : '') : '';?>" value = "<?=$old!= '' ? $old['post_heading']: '';?>"></textarea>
                        

                        <?php
                        if($errors!="" && $errors->has('post_heading')):
                        echo "<span class='error'>{$errors->first('post_heading')}</span>";
                        endif;
                        ?>

                    </div>
                </div>
                

                <div class="col-md-8 p-0 middle">
                    <div class="form-group">
                        
                        <label>Post Content</label>
                        
                        <textarea style="height:180px;" name="post_content" placeholder="Enter post content" id="post_content" class="form-control <?=$errors!='' ? ($errors->has('post_content') ? 'error is-invalid' : '') : '';?>" value = "<?=$old!= '' ? $old['post_content']: '';?>"></textarea>
                        

                        <?php
                        if($errors!="" && $errors->has('post_content')):
                        echo "<span class='error'>{$errors->first('post_content')}</span>";
                        endif;
                        ?>

                    </div>
                </div>

                
            
                <div class="col-md-8 p-0 middle">
                    <div class="form-group">
                        
                        <label>Select Image</label>
                        
                        <input type="file" name="img_name" id="img_name" data-error=".pic_error">
                        
                    </div>
                </div>

                <div class="col-md-8 p-0 middle">
                    <div class="form-group">
                        <label>Select Post Category</label>
                        <select name="category" id="category" class="form-control">
                            <?php
                                $category = $di->get('post')->getAllCategory();
                                foreach($category as $row){
                                    if($row->category_name == 'All'){

                                    }else{
                                        echo "<option value={$row->id}>{$row->category_name}</option>";
                                    }
                                    
                                }   
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-8 p-0 middle">
                    <div class="form-group">
                        
                        <label>Post Tags</label>
                        
                        <textarea style="height:40px;" name="post_tag" placeholder="Enter Tags coma(,) seprated" id="post_tag" class="form-control <?=$errors!='' ? ($errors->has('post_tag') ? 'error is-invalid' : '') : '';?>" value = "<?=$old!= '' ? $old['post_tag']: '';?>"></textarea>
                        

                        <?php
                        if($errors!="" && $errors->has('post_tag')):
                        echo "<span class='error'>{$errors->first('post_tag')}</span>";
                        endif;
                        ?>

                    </div>
                </div>

                <div class="col-md-8 middle">
                    <input type="submit" name="add_post" class="btn btn-primary" value="Submit">
                </div>
                          
            </div>
            
        </form>

        <div class="row">
            <div class="col-md-8 middle">
                <h3 style="margin-bottom:20px;">Preview</h3>
            </div>
            <div class="col-md-8 middle">    
                <h4 class="post-heading">
                    
                </h4>
            </div>
            <div class="col-md-8 middle">
                <img src="" alt="" class="preview-img">
            </div>
            <div class="col-md-8 middle">
                <p class="post-sub-content"></p>
            </div>
            <div class="col-md-8 middle">
                <p class="post-tag"></p>
            </div>
        </div>

    </div>
    
<?php 
else: 
    if(!isset($skip)){
        header("Location: http://localhost:8000/views/pages/index.php");
    }else{
        header("Location: http://localhost:8000/views/auth/signin.php");
        exit();
    }
 ?>
<?php endif; ?>
    <script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
    <script src="<?=BASEASSETS;?>scripts/pages/add-post.js"></script>
</body>
</html>