<?php
require_once "../../helper/init.php";
$authSession = "user";
Session::setSession("startPost","0");
Session::setSession("maxPost","5");
$active = "index";
$category_name = "All"; 
if(isset($_GET['category'])){
    $category_name = $_GET['category'];
}
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS;?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS;?>styles/main-card.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>styles/plugins/toastr/toaster.min.css">
</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(isset($skip) or $di->get('auth')->check()):	   
?>
    <?php
        if($category_name == "All"){
            $results = $di->get('post')->getAllPosts();
        }else{
            $results = $di->get('post')->getPostByCategory($category_name);
        }
        
        
    ?>  
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->

<?php
    if($di->get('auth')->checkAuthor()){
         require_once __DIR__."/../includes/sidebar.php" ;
    }
?>
    
    <div class="main-card">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <div class="main-post">
        <?php
                foreach($results as $row):
                    $author = $di->get('auth')->getAuthorByItsId($row->author_id);
                    $user = $di->get('auth')->getUserById($author->user_id);
        ?>
                    <div class="sub-post p-0 shadow">
                        <a href="<?=BASEPAGES;?>show-post.php?post_id=<?=$row->id;?>">
                            <div class="row special-row">
                                <div class="col-md-1"></div>   
                                <div class="col-md-5 w-h-100">
                                    <div class="post-img">
                                        <img src="<?=BASEASSETS;?>images/<?=$user->username;?>/<?=$row->img_name;?>" alt="">
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 p-t-b-15 sub-post-heading w-h-100">
                                    <h4 class="post-heading text-justify"><?=$row->post_heading;?></h4>
                                    <div class="post-content-wrapper  text-justify">
                                        <p class="post-sub-content"><?=substr($row->post_content,0,100)."...";?></p>
                                    </div>
                                    <div class="author-info">
                                        <div class="row">
                                            <div class="col-sm-6 p-0">
                                                <div class="author-info-row">
                                                    <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$user->user_img;?>">
                                                    <h6 class="author-info-name"><?=$user->username;?></h6>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 p-0">
                                                <time><?=$row->updated_at;?></time>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 p-5 w-h-100">
                                    
                                </div> -->
                                
                                
                            </div>
                        </a>
                    </div>
            <?php
                    endforeach;
            ?>
                </div>

            </div>
            <div class="col-md-2">
            <div class="card">
                <div class="card-header">
                    Category
                </div>
                <ul class="categories">
                <?php
                    $category = $di->get('post')->getAllCategory();
                    foreach($category as $row):
                ?>
                        <li class="category-list"><a class="<?= $row->category_name == $category_name ? 'active' : '';?>" href="<?=BASEURL;?>views/pages/index.php?category=<?=$row->category_name;?>"><?=$row->category_name;?></a></li>
                <?php
                    endforeach;
                ?>
                </ul>
                </div>
            </div>
        </div>
        
        
        <div data-category=<?=$category_name;?> class="more btn-more">Load More</div>
    </div>
    
    
<?php 
else: 
    header("Location: http://localhost:8000/views/auth/signin.php");
    exit();
 ?>
<?php endif; ?>

<?php
    require_once __DIR__."/../includes/page-level/index-script.php";
?>   
</body>
</html>