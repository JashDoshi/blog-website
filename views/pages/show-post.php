<?php
require_once "../../helper/init.php";
$authSession = "user";
Session::setSession("startComment","0");
Util::createCSRFToken();
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
if(isset($_GET)){
    $post_id = $_GET['post_id'];
}
$commentPlaceholder = "";
$commentEmpty = false;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>styles/plugins/toastr/toaster.min.css">

</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(isset($skip) or $di->get('auth')->check()):	   
?>
    <?php
        
        $row = $di->get('post')->getPostById($post_id);
        
    ?>  
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
    
    <div class="main-card">
    <?php
        
        $author = $di->get('auth')->getAuthorByItsId($row[0]->author_id);
        $user = $di->get('auth')->getUserById($author->user_id);
        $comment = $di->get('comment')->getAcceptedComment($author->id,$post_id);
        $tags = $di->get('post')->getAllTags($post_id);
    
        if(empty($comment)){
            $commentPlaceholder = "Be First to comment";
            $commentEmpty = true;
        }
            
?>
        <div class="main-post">
            <div class="row ">
                <div class="col-md-8 p-5 sub-post-heading middle">
                    <h4 class="post-heading"><?=$row[0]->post_heading;?></h4>
                    
                </div>
                <div class="col-md-8 p-5 middle">
                    <img src="<?=BASEASSETS;?>images/<?=$user->username;?>/<?=$row[0]->img_name;?>" alt="">
                </div>
                <div class="col-md-8 p-5 middle">
                    <p class="post-sub-content"><?=$row[0]->post_content;?></p>
                    
                </div>
                <div class="col-md-8 p-5 middle">
                    <div class="post-tags">
                    
<?php   
                if(!empty($tags)):
                    foreach($tags as $tag):
?>
                        <span><a href="#" style="color:blue!important"><?=$tag->tag_name;?></a> </span>
<?php
                    endforeach;
                endif;
?>
                    </div>
                </div>
                <div class="col-md-8 p-5 middle">
                    <div class="author-info m-t-20">
                        <div class="author-info-row">
                            <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$user->user_img;?>">
                            <h6 class="author-info-name"><?=$user->username;?></h6>
                        </div>
                        <time><?=$row[0]->updated_at;?></time>
                    </div>
                </div>
                <div class="col-md-8 p-5 middle">
                    <form action="<?=BASEURL;?>helper/routing.php" method="post" class="form-horizontal" id="commentForm" role="form"> 
                        <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
            
                        <input type="hidden" name="author_id" id="author_id" value="<?=$author->id?>">
                        <input type="hidden" name="user_id" id="user_id" value="<?=Session::getSession('user')?>">
                        <input type="hidden" name="post_id" id="post_id" value="<?=$post_id?>">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label p-0">Comment</label>
                            <div class="col-sm-10 p-0">
                              <textarea placeholder="<?=$commentPlaceholder;?>" class="form-control" name="comment_content" id="comment_content" rows="3"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group p-0">
                            <div class="col-sm-offset-2 col-sm-10 p-0">  
<?php
                            if(isset($skip)):
?>
                                <a href="<?=BASEAUTH?>signin.php" class="btn btn-success btn-circle text-uppercase">Sign in to comment</a>
<?php
                            else:
?>                  
                                <button class="btn btn-success btn-circle text-uppercase" type="submit" name="submitComment" id="submitComment">Summit comment</button>
<?php   
                            endif;
?>
                            </div>
                        </div>            
                    </form>
                </div>
                   
            </div>
<?php
        if(!$commentEmpty):
?>
            <div>
                <h4 class="middle text-center">Some Comments</h4>
                <div class="single-comment row">
    <?php
            
                foreach($comment as $row):
                    $commenterUser = $di->get('auth')->getUserById($row->user_id);
                
    ?>
                    
                    <div class="col-md-8 p-5 middle shadow">
                        <div class="row">
                            <div class="author-info m-t-20 col-md-4">
                                <div class="author-info-row">
                                    <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$commenterUser->user_img;?>">
                                    <h6 class="author-info-name"><?=$commenterUser->username;?></h6>
                                </div>
                                <time><?=$row->updated_at;?></time>
                            </div>
                            <p class="post-sub-content col-md-8"><?=$row->comment;?></p>
                        </div>
                    </div>
    <?php
                endforeach;
           
    ?>
            </div>
            <div class="col-md-8 middle p-5">
                <div class="more-comment btn-more" data-author="<?=$author->id;?>" data-post="<?=$post_id;?>">
                    More Comments    
                </div>   
            </div>
<?php
            endif;
?>
            </div>  
            
        </div>
    </div>
    
<?php 
else: 
    header("Location: http://localhost:8000/views/auth/signin.php");
    exit();
 ?>
<?php endif; ?>
    <script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
    <script src="<?=BASEASSETS;?>scripts/plugins/toastr/toaster.min.js"></script>
    <script src="<?=BASEASSETS;?>scripts/pages/show-post.js"></script>
</body>
</html>