<?php
require_once "../../helper/init.php";
$authSession = "user";
Util::createCSRFToken();
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/comments.css">
</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(!isset($skip) or $di->get('auth')->check()):
?>
      
    
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
    
    <div class="main-card">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="tab-pane" id="account-settings">
                    <a class="float-right btn-index m-10" href="<?=BASEAUTH;?>signout.php">signout</a>  
                    <form enctype="multipart/form-data" action="<?=BASEURL;?>helper/routing.php" method="post" class="form-horizontal" id="accountSetForm" role="form">
                        <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                        <div class="form-group">
                            <label for="avatar" class="col-sm-2 control-label">Profile</label>
                            <div class="col-sm-10">                                
                                <div class="custom-input-file">
                                    <img src="<?=BASEASSETS;?>images/users/<?=$user->user_img?>" alt="">
                                    <label class="uploadPhoto">
                                        Edit
                                        <input type="file" class="change-avatar" name="user_img" id="user_img">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">User Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username" id="username" value="<?=$user->username;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="first_name" class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="first_name" id="first_name" value=<?=$user->first_name;?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="last_name" id="last_name" value=<?=$user->last_name;?>>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" id="email" value=<?=$user->email;?>>
                            </div>
                        </div> 
                    
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">                    
                                <button name="edit-profile" class="btn btn-primary btn-circle text-uppercase" type="submit" id="submit">Save changes</button>
                            </div>
                        </div>            
                    </form>
                    <?php
                        if($di->get('auth')->getAuthorById(Session::getSession($authSession))==null):
                    ?>
                            <p class="be-author">To be an Author <a href=""  data-toggle="modal" data-target="#beAuthor" class="btn-index">Subscribe</a></p> 
                    <?php
                        endif;
                    ?>
                </div>
            </div>
        </div>
         
        
    </div>
    <!-- Modal -->
    <div class="modal fade" id="beAuthor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Delete Comment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <form method="POST" action="<?=BASEURL;?>helper/routing.php">
                    <div class="modal-body">
                        <input type="hidden" name="csrf_token" id="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                        <input type="hidden" name="id" id="id" value="<?=Session::getSession('user');?>">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                Please pay this much to be an Author
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" name="subscribe" class="btn btn-success">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
<?php 
else: 
    header("Location: http://localhost:8000/views/auth/signin.php");
    exit();
 ?>
<?php endif; ?>
    <script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
    <script src="<?=BASEASSETS;?>scripts/bootstrap/bootstrap.min.js"></script>
</body>
</html>