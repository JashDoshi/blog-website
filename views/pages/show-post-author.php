<?php
require_once "../../helper/init.php";
$authSession = "user";
Session::setSession("startComment","0");
Util::createCSRFToken();
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
if(isset($_GET)){
    $post_id = $_GET['post_id'];
}
$commentPlaceholder = "";
$commentEmpty = false;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/comments.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>vendor/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>styles/plugins/toastr/toaster.min.css">

</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(!isset($skip) and $di->get('auth')->checkAuthor()):	   
?>
    <?php
        
        $row = $di->get('post')->getPostById($post_id);
        
    ?>  
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
    
    <div class="main-card">
    <?php
        
        $author = $di->get('auth')->getAuthorByItsId($row[0]->author_id);
        $user = $di->get('auth')->getUserById($author->user_id);
        $comment = $di->get('comment')->getAcceptedComment($author->id,$post_id);
        if(empty($comment)){
            $commentEmpty = true;
        }
            
?>
        <div class="main-post">
            
            <button data-toggle="modal" data-target="#deletePost" data-id="<?=$post_id;?>" class='btn btn-outline-danger btn-sm btn-comment-delete float-right' style="position:relative;top:70px;width:40px;height:40px" ><i class='fa fa-trash' style="font-size:24px"></i></button>
        
            <form enctype="multipart/form-data" action="<?=BASEURL;?>helper/routing.php" method="post" class="form-horizontal"  role="form">
                <div class="row ">
                    
                    <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                    <input type="hidden" name="post_id" value="<?=$post_id;?>">
                    <input type="hidden" name="author_id" value="<?=$author->id;?>">
                    
                    <div class="col-md-8 p-5 sub-post-heading middle">
                        <textarea class="post-heading" name="post_heading" id="post_heading"><?=$row[0]->post_heading;?></textarea>
                    
                    </div>
                    <div class="col-md-8 p-5 middle custom-input-file-post">
                        <img src="<?=BASEASSETS;?>images/<?=$user->username;?>/<?=$row[0]->img_name;?>" alt="">
                        <label class="uploadPost">
                            Edit
                            <input type="file" class="post_img" name="post_img" id="post_img">
                        </label>
                    </div>
                    <div class="col-md-8 p-5 middle">
                        <textarea class="post-sub-content" name="post_content" id="post_content" style="width:100%;height:50vh;" ><?=$row[0]->post_content;?></textarea>
                        
                        <div class="author-info m-t-20">
                            <div class="author-info-row">
                                <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$user->user_img;?>">
                                <h6 class="author-info-name"><?=$user->username;?></h6>
                            </div>
                            <time><?=$row[0]->updated_at;?></time>
                        </div>
                    </div>
                    <div class="col-md-8 middle p-5">
                        <div class="form-group">         
                            <button name="edit-post" class="btn btn-primary btn-circle text-uppercase" type="submit" id="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
                
<?php
                if(!$commentEmpty):
?>
                    <div>
                        <h4 class="middle text-center">Some Comments</h4>
                        <div class="single-comment row">
<?php
                    
                        foreach($comment as $row):
                            $commenterUser = $di->get('auth')->getUserById($row->user_id);
                        
?>
                            
                            <div class="col-md-8 p-5 middle shadow">
                                <div class="row">
                                    <div class="author-info m-t-20 col-md-4">
                                        <div class="author-info-row">
                                            <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$commenterUser->user_img;?>">
                                            <h6 class="author-info-name"><?=$commenterUser->username;?></h6>
                                        </div>
                                        <time><?=$row->updated_at;?></time>
                                    </div>
                                    <p class="post-sub-content col-md-7"><?=$row->comment;?></p>
                                    <div class="col-md-1">
                                        <button data-toggle="modal" data-target="#deleteCommentModal" class='btn btn-outline-danger btn-sm btn-comment-delete' data-id='<?=$row->id;?>'><i class='fa fa-trash'></i></button> 
                                    </div>
                                </div>
                            </div>
<?php
                        endforeach;
                
?>
                    </div>
                    <div class="col-md-8 middle p-5">
                        <div class="more-comment btn-more" data-author="<?=$author->id;?>" data-post="<?=$post_id;?>">
                            More Comments    
                        </div>   
                    </div>
<?php
                else:
?>
                    <div class="row">
                        <div class="col-md-8 middle">
                            <h4 class="middle">No Comments Yet</h4>
                        </div>
                    </div>
                    
<?php
                endif;
?>
                    </div> 
        
            
            
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Delete Comment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <form method="POST" action="<?=BASEURL;?>helper/routing.php">
                    <div class="modal-body">
                        <input type="hidden" name="csrf_token" id="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                Are you sure you want to delete this comment?
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" name="delete-comment" class="btn btn-success">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deletePost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Delete Post</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <form method="POST" action="<?=BASEURL;?>helper/routing.php">
                    <div class="modal-body">
                        <input type="hidden" name="csrf_token" id="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                Are you sure you want to delete this Post?
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" name="delete-comment" class="btn btn-success">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php 
else: 

    if(!isset($skip)){
        header("Location: http://localhost:8000/views/pages/index.php");
    }else{
        header("Location: http://localhost:8000/views/auth/signin.php");
        exit();
    }
 ?>
<?php endif; ?>
    <script src="<?=BASEASSETS;?>scripts/jquery.js"></script>
    <script src="<?=BASEASSETS;?>scripts/bootstrap/bootstrap.min.js"></script>
    <script src="<?=BASEASSETS;?>scripts/plugins/toastr/toaster.min.js"></script>
    <script src="<?=BASEASSETS;?>scripts/pages/show-post-author.js"></script>
</body>
</html>