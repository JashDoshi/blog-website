<?php
require_once "../../helper/init.php";
$authSession = "user";
Util::createCSRFToken();

$active = "manage-post";
if(Session::hasSession("skip")){
    $skip = Session::getSession("skip");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/bootstrap/bootstrap2.min.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/sidebar.css">
    <link rel="stylesheet" href="<?=BASEASSETS?>styles/main-card.css">
</head>
<body>
<?php
//Util::dd($_SESSION['user']);
if(!isset($skip) and $di->get('auth')->checkAuthor()):	   
?>
<?php
    $user = $di->get('auth')->getUserById(Session::getSession('user'));
    
    $results = $di->get('post')->getPosts($di->get('auth')->getAuthorById($_SESSION['user'])->id);
    // Util::dd($results);
?>    
    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/header.php" ?>
    <!-- End of Sidebar -->


	<!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->
  
    <div class="main-card">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
<?php
    foreach($results as $row):
        
?>
        <div class="sub-post2">
            <a href="<?=BASEPAGES;?>show-post-author.php?post_id=<?=$row->id;?>">
                <div class="row">
                    <div class="col-md-4 p-5 sub-post-heading w-h-100">
                        <h4 class="post-heading"><?=$row->post_heading;?></h4>
                        <div class="author-info">
                            <div class="author-info-row">
                                <img class="author-img" src="<?=BASEASSETS;?>images/users/<?=$user->user_img;?>">
                                <h6 class="author-info-name"><?=$user->username;?></h6>
                            </div>
                            <time><?=$row->updated_at;?></time>
                        </div>
                    </div>
                    <div class="col-md-4 p-5 w-h-100">
                        <p class="post-sub-content"><?=substr($row->post_content,0,200)."...";?></p>
                    </div>
                    <div class="col-md-4 p-5 w-h-100">
                        <img src="<?=BASEASSETS;?>images/<?=$user->username;?>/<?=$row->img_name;?>" alt="">
                    </div>
                    
                </div>
            </a>
        </div>
<?php
        endforeach;
?>

        </div>
    </div>
        </div>
    </div>
    
<?php 
else: 
    if(!isset($skip)){
        header("Location: http://localhost:8000/views/pages/index.php");
    }else{
        header("Location: http://localhost:8000/views/auth/signin.php");
        exit();
    }
 ?>
<?php endif; ?>
</body>
</html>