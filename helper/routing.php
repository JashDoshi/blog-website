<?php
require_once "init.php";
if(isset($_GET['indexPage']))
{
	if($_GET['category_name'] == 'All'){
		$results = $di->get('post')->getAllPosts();
	}else{
		$results = $di->get('post')->getPostByCategory($_GET['category_name']);
	}
	
	echo json_encode($results);
}
if(isset($_GET['more-comment']))
{
	$results = $di->get('comment')->getAcceptedComment($_GET['author_id'],$_GET['post_id']);
	$resultArray = $di->get('comment')->getUserByComment($results);
	echo json_encode($resultArray);
}
if(isset($_POST['skip']))
{
    Session::setSession("skip","true");
    Util::redirect("index.php");
    exit;
    
}
if(isset($_POST['submitComment'])){
	if(Util::verifyCSRFToken($_POST))
	{
		
		$author = $di->get('auth')->getAuthorByItsId($_POST['author_id']);
		$user = $di->get('auth')->getUserById($author->user_id);
		$commentSender = $di->get('auth')->getUserById($_POST['user_id']);
		$body = "<p>$commentSender->username has Commented on your Post</p>";
		$body .= "<p>Please Check Your Comment Box</p>";
		// Util::dd($user->email);
		$mail->addAddress($user->email);
		$mail->Subject = "New Comment";
		$mail->Body = $body;

		if($mail->send())
		{
			$result = $di->get('comment')->addComment($_POST);
			switch($result)
			{
				case ADD_ERROR:
					Session::setSession(ADD_ERROR,"Add Post Error!");
					Util::redirect("index.php");
					break;
				case ADD_SUCCESS:
					Session::setSession(ADD_COMMENT,"Add Comment Success!");
					Util::redirect("index.php");
					break;
				case VALIDATION_ERROR:
					Session::setSession('validation',"Validation Error");
					Session::setSession('old',$_POST);
					Session::setSession('errors',serialize($di->get('post')->getValidator()->errors()));
					Util::redirect("show-post.php");
					break;
			}
		}else{
			Util::dd($mail->ErrorInfo);
		}
		
	}
	else
	{
		Util::dd("csrf error");
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("index.php");//need to redirect to some error page
	}
}
if(isset($_POST['add_post']))
{
//    Util::dd($_POST['post_img']);
	if(Util::verifyCSRFToken($_POST))
	{
		$user = $di->get('auth')->getUserById(Session::getSession('user'));
		
        $img_id;
        if($di->get('post')->getPostCount($_POST["author_id"]) == "0"){
			$img_id = 1;
			mkdir("../assets/images/".$user->username);
        }
        else{
            $img_id = $di->get('post')->getPostCount($_POST["author_id"])+1;
        } 
        $image_name = $user->username.$img_id;
        
        
        if(isset($_FILES['img_name']['name']))
        {
            $file_name = $_FILES['img_name']['name'];
            $file_tmp = $_FILES['img_name']['tmp_name'];

            $temp = explode(".", $file_name);
            $file_extension = strtolower(end($temp));

            $image_name .= "." . $file_extension;
            move_uploaded_file($file_tmp, "../assets/images/{$user->username}/{$image_name}");
        }
        
		$result = $di->get('post')->addPost($_POST,$image_name);
		switch($result)
		{
			case ADD_ERROR:
				Session::setSession(ADD_ERROR,"Add Post Error!");
				Util::redirect("index.php");
				break;
			case ADD_SUCCESS:
				Session::setSession(ADD_POST,"Add Post Success!");
				Util::redirect("index.php");
				break;
			case VALIDATION_ERROR:
				Session::setSession('validation',"Validation Error");
				Session::setSession('old',$_POST);
				Session::setSession('errors',serialize($di->get('post')->getValidator()->errors()));
				Util::redirect("add-post.php");
				break;
		}
	}
	else
	{
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("index.php");//need to redirect to some error page
	}
}
if(isset($_POST['edit-post']))
{
	if(Util::verifyCSRFToken($_POST))
	{
		$author = $di->get('auth')->getAuthorByItsId($_POST['author_id']);
		$post = $di->get('post')->getPostById($_POST['post_id']);
		$image_name = $post[0]->img_name;
		$post_id = $post[0]->id;
		if(isset($_FILES['post_img']['name']) and strlen($_FILES['post_img']['name'])!=0)
		{
			
			$file_name = $_FILES['post_img']['name'];
			$file_tmp = $_FILES['post_img']['tmp_name'];

			move_uploaded_file($file_tmp, "../assets/images/{$author->username}/{$image_name}");
		}
	
		$result = $di->get('post')->updatePost('posts',$_POST,"id = $post_id",$image_name);
		
		switch($result)
		{
			case UPDATE_ERROR:
				Session::setSession(UPDATE_ERROR,"Update Profile Error!");
				Util::redirect("index.php");
				break;
			case UPDATE_POST:
				Session::setSession(UPDATE_POST,"Post Updated Successfully");
				Util::redirect("index.php");
				break;
			case VALIDATION_ERROR:
				Session::setSession('validation',"Validation Error");
				Session::setSession('old',$_POST);
				Session::setSession('errors',serialize($di->get('post')->getValidator()->errors()));
				Util::redirect("add-post.php");
				break;
		}
        
	}
	else
	{
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("index.php");//need to redirect to some error page
	}
}
if(isset($_POST['edit-profile']))
{
	if(Util::verifyCSRFToken($_POST))
	{
		
		$user = $di->get('auth')->getUserById(Session::getSession('user'));
		$image_name = $user->user_img;
		
		if(isset($_FILES['user_img']['name']) and strlen($_FILES['user_img']['name'])!=0)
		{
			if(strcmp($user->user_img, "default_user.png")==0){
				$image_name = "default-user";
			}else{
				$image_name = $user->username;
			}
			$file_name = $_FILES['user_img']['name'];
			$file_tmp = $_FILES['user_img']['tmp_name'];

			$temp = explode(".", $file_name);
			$file_extension = strtolower(end($temp));

			$image_name .= "." . $file_extension;
			move_uploaded_file($file_tmp, "../assets/images/users/{$image_name}");
		}
	
		$result = $di->get('auth')->updateUser('users',$_POST,"id = $user->id",$image_name);
		switch($result)
		{
			case UPDATE_ERROR:
				Session::setSession(UPDATE_ERROR,"Update Profile Error!");
				Util::redirect("index.php");
				break;
			case UPDATE_PROFILE:
				Session::setSession(UPDATE_PROFILE,"Profile Updated Successfully");
				Util::redirect("index.php");
				break;
			case VALIDATION_ERROR:
				Session::setSession('validation',"Validation Error");
				Session::setSession('old',$_POST);
				Session::setSession('errors',serialize($di->get('post')->getValidator()->errors()));
				Util::redirect("add-post.php");
				break;
		}
        
	}
	else
	{
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("index.php");//need to redirect to some error page
	}
}
if(isset($_POST['comment']))
{
	$search_parameter = $_POST['search']['value'] ?? null;
	$start = $_POST['start'];
	$length = $_POST['length'];
	$draw = $_POST['draw'];
	$di->get('comment')->getJSONDataForDataTable($draw,$search_parameter,$start,$length);
}
if(isset($_POST['accept_comment']))
{
	$comment_id = $_POST['comment_id'];
	$result = $di->get('comment')->acceptComment($comment_id);
	switch($result)
	{
		case UPDATE_ERROR:
			Session::setSession(UPDATE_ERROR,"Update Comment Error!");
			Util::redirect("index.php");
			break;
		case UPDATE_SUCCESS:
			Session::setSession(UPDATE_COMMENT,"Comment has added to your post!");
			break;
		case VALIDATION_ERROR:
			Session::setSession('validation',"Validation Error");
			Session::setSession('old',$_POST);
			Session::setSession('errors',serialize($di->get('category')->getValidator()->errors()));
			Util::redirect("show-comments.php");
			break;
	}
}
if(isset($_POST['reject_comment']))
{
	$comment_id = $_POST['comment_id'];
	$author_name = $_POST['username'];
	$email = $_POST['email'];
	$result = $di->get('comment')->rejectComment($comment_id);
	switch($result)
	{
		case UPDATE_ERROR:
			Session::setSession(UPDATE_ERROR,"Update Comment Error!");
			Util::redirect("index.php");
			break;
		case UPDATE_SUCCESS:
			$body = "<p>$author_name has Rejected your Commented</p>";
			$body .= "<p>Please comment something nice</p>";
			// Util::dd($user->email);
			$mail->addAddress($email);
			$mail->Subject = "New Comment";
			$mail->Body = $body;
			if($mail->send()){
				Session::setSession(REJECT_COMMENT,"Comment has been rejected");
			}
			
			break;
		case VALIDATION_ERROR:
			Session::setSession('validation',"Validation Error");
			Session::setSession('old',$_POST);
			Session::setSession('errors',serialize($di->get('category')->getValidator()->errors()));
			Util::redirect("show-comments.php");
			break;
	}
}
if(isset($_POST['subscribe'])){
	if(Util::verifyCSRFToken($_POST))
	{
		$result = $di->get('auth')->subscribe($_POST['id']);
		switch($result){
			case ADD_AUTHOR:
				Session::setSession(ADD_AUTHOR,"You are now an author");
				Util::redirect("index.php");
				break;
			case ADD_ERROR:
				Session::setSession(ADD_ERROR,"Issue while payment, please try Again!!");
				Util::redirect("index.php");
				break;
		}
	}
	else
	{
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("index.php");//need to redirect to some error page
	}
}