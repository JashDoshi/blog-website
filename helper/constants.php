<?php

define('BASEURL', $di->get('config')->get('base_url'));
define('BASEASSETS', BASEURL."assets/");
define('BASEAUTH', BASEURL."views/auth/");
define('BASEPAGES', BASEURL."views/pages/");

define('ADD_POST', 'add post success');
define('ADD_AUTHOR', 'subscribed successfully');
define('ADD_SUCCESS','add success');
define('ADD_COMMENT','add comment success');
define('UPDATE_COMMENT','updated comment');
define('UPDATE_PROFILE','updated profile');
define('UPDATE_POST','updated post');
define('ADD_ERROR','add error');
define('UPDATE_SUCCESS', 'update success');
define('UPDATE_ERROR','update error');
define('DELETE_SUCCESS', 'delete success');
define('DELETE_ERROR','delete error');
define('VALIDATION_ERROR','validation error');
define('REJECT_COMMENT','reject comment');