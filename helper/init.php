<?php
ini_set('display_errors','On');
date_default_timezone_set('Asia/Kolkata');
error_reporting(E_ALL);
session_start();

require_once __DIR__ ."/requirements.php";

$di = new DependencyInjector();
$di->set('config',new Config());
$di->set('database',new Database($di));
$di->set('hash',new Hash());
$di->set('util',new Util($di));
$di->set('error_handler',new ErrorHandler());
$di->set('validator',new Validator($di));
$di->set('auth',new Auth($di));
$di->set('token_handler',new TokenHandler($di));
$di->set('post',new Post($di));
$di->set('comment',new Comment($di));
$mail = MailConfigHelper::getMailer();

$di->get('auth')->build();
$di->get('token_handler')->build();
if(isset($_COOKIE['token']) && $di->get('token_handler')->isValid($_COOKIE['token'],1))
{
    $user = $di->get('token_handler')->getUserFromValidToken($_COOKIE['token']);
    $di->get('auth')->setAuthSession($user->id);
}
require_once __DIR__."/constants.php";