
var TableDatatables = function(){
	var handleCategoryTable = function(){
        
		var manageCategoryTable = $("#manage-comment-table");
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		var oTable = manageCategoryTable.DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: baseURL+filePath,
				type: "POST",
				data: {
					"comment": "comment"
				}
			},
			"lengthMenu": [
				[5, 15, 25, -1], //indexing
				[5, 15, 25, "All"] //to show at UI
			],
			"order": [
				[1, "desc"]
			],
			"columnDefs": [ //for which column we dont want sorting
				{
					"orderable": false,
					"targets": [0, -1]
				}
			]
		});
		manageCategoryTable.on('click','.accept',function(e){
			var id = $(this).data('id');
			$.ajax({
				url: baseURL+filePath,
				method: "POST",
				data: {
					"comment_id": id,
					"accept_comment": "accept"
				},
				success: function(response){
					window.location = baseURL+"/views/pages/index.php";
				}
			});
		});
		manageCategoryTable.on('click','.reject',function(e){
			var id = $(this).data('id');
			var username = $(this).data('name');
			var email = $(this).data('email');
			$.ajax({
				url: baseURL+filePath,
				method: "POST",
				data: {
					"comment_id": id,
					"username": username,
					"email": email,
					"reject_comment": "reject"
				},
				success: function(response){
					window.location = baseURL+"/views/pages/index.php";
				}
			});
		});
	}
	return {
		//main function to handle all the datatables
		init: function(){
			handleCategoryTable();
		}
	}
}();
jQuery(document).ready(function(){
	TableDatatables.init();
});