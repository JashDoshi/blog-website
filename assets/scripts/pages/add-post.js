$(document).ready(function() {
    $("#post_heading").keyup(function(e) {
        var code = e.keyCode;
        if(code == 13) { 
            var currentVal = $('#post_heading').val();
            $('#post_heading').val(currentVal+"<br>");
            
            var input = $('#post_heading').val();
            $('.post-heading').html(input);
        }else{
            var input = $('#post_heading').val();
            $('.post-heading').html(input);
        }
        
    });
    $("#post_content").keyup(function(e) {
        var code = e.keyCode;
        if(code == 13) { 
            var currentVal = $('#post_content').val();
            $('#post_content').val(currentVal+"<br>");
            
            var input = $('#post_content').val();
            $('.post-sub-content').html(input);
        }else{
            var input = $('#post_content').val();
            $('.post-sub-content').html(input);
        }
    });
    $("#post_tag").keyup(function(e) {
        var code = e.keyCode;
        if(code == 13) { 
            var currentVal = $('#post_tag').val();
            $('#post_tag').val(currentVal+"<br>");
            
            var input = $('#post_tag').val();
            $('.post-tag').html(input);
        }else{
            var input = $('#post_tag').val();

            var tag = input.split(",");
            $('.post-tag').html($('.post-tag').val());
            tag.forEach(str => {
                $('.post-tag').append("#"+str+" ");
            });
            // $('.post-tag').html(input);
        }
    });
    $("#img_name").change(function(e){
        var tmppath = URL.createObjectURL(e.target.files[0]);
        console.log(URL.createObjectURL(e.target.files[0]));
        $(".preview-img").fadeIn("slow").attr('src',tmppath);
    });
});