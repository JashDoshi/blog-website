$(document).ready(function() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $(".more").click(function(e) {
        var category_name = $(this).data('category');
        
        $.ajax({
            type: "GET",
            url: baseURL+filePath,
            data: {
                'indexPage': "true",
                'category_name': category_name
            },
            success: function(response){
                var obj = JSON.parse(response);
                var len = obj.length;
                if(len==0){
                    $(".more").css("display","none");
                    toastr.info("It seems there are no blogs left","Sorry!");
                }
                else{
                    for(var i=0; i<len; i++){
                        $(".main-post").append(`
                        
                        <div class="sub-post p-0 shadow">
                            <a href="${baseURL}/views/pages/show-post.php?post_id=${obj[i].id}">
                                <div class="row special-row">
                                    <div class="col-md-1"></div>   
                                    <div class="col-md-5 w-h-100">
                                        <div class="post-img">
                                            <img src="${baseURL}/assets/images/${obj[i].username}/${obj[i].img_name}" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-t-b-15 sub-post-heading w-h-100">
                                        <h4 class="post-heading">${obj[i].post_heading}</h4>
                                        <div class="post-content-wrapper">
                                            <p class="post-sub-content">${(obj[i].post_content).substring(0,100)+"...."}</p>
                                        </div>
                                        <div class="author-info">
                                            <div class="row">
                                                <div class="col-sm-6 p-0">
                                                    <div class="author-info-row">
                                                        <img class="author-img" src="${baseURL}/assets/images/users/${obj[i].user_img}">
                                                        <h6 class="author-info-name">${obj[i].username}</h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 p-0">
                                                    <time>${obj[i].updated_at}</time>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4 p-5 w-h-100">
                                        
                                    </div> -->
                                    
                                    
                                </div>
                            </a>
                        </div>
                        `);
                    }
                }  
            }
        });
    })
});