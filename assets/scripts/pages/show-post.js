$(document).ready(function() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $(".more-comment").click(function(e) {
        var author_id = $(this).data('author');
        var post_id = $(this).data('post');
        $.ajax({
            type: "GET",
            url: baseURL+filePath,
            data: {
                'more-comment': "true",
                'author_id': author_id,
                'post_id': post_id
            },
            success: function(response){
                var obj = JSON.parse(response);
                var len = obj.length;
                
                if(len==0){
                    $(".more-comment").css("display","none");
                    toastr.info("This was the last Comment","End of Comment");
                }else{
                    for(var i=0; i<len; i++){
                    
                        for(var j=0;j<obj[i].length;j++){
                            $(".single-comment").append(`
                            <div class="col-md-8 p-5 middle shadow">
                                <div class="row">
                                    <div class="author-info m-t-20 col-md-4">
                                        <div class="author-info-row">
                                            <img class="author-img" src="${baseURL}/assets/images/users/${obj[i][j].user_img}">
                                            <h6 class="author-info-name">${obj[i][j].username}</h6>
                                        </div>
                                        <time>${obj[i][j].updated_at}</time>
                                    </div>
                                    <p class="post-sub-content col-md-8">${obj[i][j].comment}</p>
                                </div>
                            </div>
                            `);
                        }
                    }
                }
            }
        });
    })
});